extends KinematicBodyPlus

export var falling_speed = 0.98
export var max_falling_speed = 98.0

var vel: Vector3 = Vector3()
var vel_prev: Vector3 = vel


func _fall(delta):
	if vel.y > -max_falling_speed:
		vel.y -= falling_speed


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	pass


func _physics_process(delta):
	vel_prev = vel

	var forward: float = float(Input.is_key_pressed(KEY_S)) - float(Input.is_key_pressed(KEY_W))
	var side: float = float(Input.is_key_pressed(KEY_D)) - float(Input.is_key_pressed(KEY_A))

	vel += $Head.transform.basis.z * forward
	vel += $Head.transform.basis.x * side

	var turn: float = float(Input.is_key_pressed(KEY_LEFT)) - float(Input.is_key_pressed(KEY_RIGHT))
	$Head.rotation.y += turn * 4.0 * delta

	if Input.is_key_pressed(KEY_UP) && $Head/Camera.rotation_degrees.x < 90.0:
		$Head/Camera.rotation.x += 4.0 * delta
	elif Input.is_key_pressed(KEY_DOWN) && $Head/Camera.rotation_degrees.x > -90.0:
		$Head/Camera.rotation.x -= 4.0 * delta

	vel.x = lerp(vel.x, 0.0, delta * 5.0)
	vel.z = lerp(vel.z, 0.0, delta * 5.0)

	var snap: Vector3 = Vector3.DOWN / 3.0

	if is_on_floor():
		if Input.is_key_pressed(KEY_SPACE):
			global_transform.origin -= snap
			snap = Vector3()
			vel.y += 20.0
	else:
		_fall(delta)

	# disGUSTING workaround for corner jittering on concave/trimesh collisionshapes
	# note to self: i think this has something to do with move_and_slide()'s
	# code where the vector that results from colliding with a wall is done by
	# lv.slide(wall normal) and some kind of imprecision when it comes to that
	# or there is still some left over movement in an unwanted axis after lv.slide()
	# that makes them jitter in corners so you have to calculate the velocity
	# separately per-axis
	vel.x = move_and_slide_with_snap(Vector3(vel.x, 0.0, 0.0), snap, Vector3.UP, true, 1).x
	vel.y = move_and_slide_with_snap(Vector3(0.0, vel.y, 0.0), snap, Vector3.UP, true, 1).y
	vel.z = move_and_slide_with_snap(Vector3(0.0, 0.0, vel.z), snap, Vector3.UP, true, 1).z
	pass


func _input(event):
	if event is InputEventMouseMotion && Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		$Head.rotate_y(-event.relative.x * 0.005)
		$Head/Camera.rotate_x(-event.relative.y * 0.005)
		$Head/Camera.rotation_degrees.x = clamp($Head/Camera.rotation_degrees.x, -90.0, 90.0)
	pass
